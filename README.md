# Elixir and Nerves beginner workshop

This is the speaker repository.

## How am I going to do this?

Obviously this will be a iterative project

1.  try to create a detailed agenda (as detailed as possible)
2.  create the pomodoro application + hw that we are going to make. See how long I think this will take.
3.  fill in all details of the agenda in more and more detail until we are done

## Agenda

- Introduction to elixir
- Basic concepts of elixir
- Playing in the REPL
- Basics of OTP (GenServers!)
- Installing nerves on a raspberry pi
- Lighting a led with elixir
- Implementation of a pomodoro timer
- Creating a circuit for the pomodoro timer

### Detail

- 09:00-09:10: installing in groups of 3 per table
- 09:10-09:20: explain the workshop, explain some concepts
- 09:20-10:30: introduction to elixir, till modules and functions
- 10:30-10:40: break
- 10:40-12:30: bring into practice what we learned: first test driven version of pomodoro (per team)

goal for lunch: have a test driven pomodoro

- 12:30-13:30: break
- 13:30-15:00: genserver + nerves -> lighting a led (individually)

goal for first session: lid a led via nerves and IEx

- 15:00-15:10: break
- 15:10-16:40: continue, adapt the breadboard for pomodoro
- 16:40-16:50: break
- 16:50-18:20: continue, pomodoro, if time: TTD hardware, api/web application, distributed pomodoro

## IEx

```elixir
IO.puts "Welcome at the workshop"
IO.puts("Welcome at the workshop")
```

We can also put this in a script folder

## In the workshop:

- room for questions (question, doing, done)
- room for technical debt

## Questions:

- Maybe some more multimeters would be nice

# Implemetation

## TDD pomodoro

> The Pomodoro Technique is a time management method developed by Francesco Cirillo in the late 1980s.[1] The technique uses a timer to break down work into intervals, traditionally 25 minutes in length, separated by short breaks. These intervals are named pomodoros, the plural in English of the Italian word pomodoro (tomato), after the tomato-shaped kitchen timer that Cirillo used as a university student.[2][3]
> The technique has been widely popularized by dozens of apps and websites providing timers and instructions. Closely related to concepts such as timeboxing and iterative and incremental development used in software design, the method has been adopted in pair programming contexts.[4] > https://en.wikipedia.org/wiki/Pomodoro_Technique

Our pomodoro has 2 states: _working_ and _pauzing_.

Some GWTs:

```cucumber
WHEN I start a pomodoro timer
THEN the pomodoro timer is in working

GIVEN I started a pomodoro timer
WHEN 24 minutes have passed
THEN the pomodoro timer is in working

GIVEN I started a pomodoro timer
WHEN 25 minutes have passed
THEN the pomodoro timer is in pauzing

GIVEN I have a pomodoro timer in pauzing
WHEN 5 minutes have passed
THEN the pomodoro timer is in working
```

optional extra (stopping):

```cucumber
GIVEN I have a pomodoro timer in working
WHEN I stop the pomodoro timer
THEN the pomodoro timer is stopped

GIVEN I have a pomodoro timer in pauzing
WHEN I stop the pomodoro timer
THEN the pomodoro timer is stopped
```

optional scenario (postpone):

```cucumber
GIVEN I have a pomodoro timer in working for 24 minutes
WHEN I postpone the timer for 5 minutes
AND 1 minute is passed (25 minutes in total)
THEN the pomodoro timer is in working

GIVEN I have a pomodoro timer in working for 24 minutes
WHEN I postpone the timer for 5 minutes
AND 6 minutes are passed (30 minutes in total)
THEN the pomodoro timer is in pauzing

GIVEN I have a pomodoro timer in pauzing for 3 minutes
WHEN I postpone the timer for 2 minutes
AND 3 minutes are passed (6 minutes in total)
THEN the pomodoro timer is in pauzing

GIVEN I have a pomodoro timer in pauzing for 3 minutes
WHEN I postpone the timer for 2 minutes
AND 4 minutes are passed (7 minutes in total)
THEN the pomodoro timer is in working
```

nice things for the leds:

```cucumber
GIVEN I have a pomodoro timer in working
WHEN the time of timer is almost up
THEN the led indicating working start blinking

GIVEN I have a pomodoro timer in pauzing
WHEN the time of timer is almost up
THEN the led indicating pauzing start blinking

GIVEN I have not started a pomodor timer
WHEN I press start
THEN the leds indicating pauzing and working blink briefly before turning on working

GIVEN I have a pomodoro timer
WHEN I press stop
THEN the leds indicating pauzing and working blink briefly before turning completely off
```

## starting the implementation

```bash
mix new pomodoro
cd pomodoro
mix test
```

adding `test.watch` https://github.com/lpil/mix-test.watch

`mix test.watch`

## remarks about the implemetation

### `state` over `is_working?/is_pauzing?`

State can only return one thing, you can't cheat.
is_working/is_pauzing can both return true

### At what point in time should I give them the api?

I think it would be nice to give them the fact that they should have an advance_time function

Start really simple: do everything explicit, later add the timer and then add the listener

### don't do anything with the state you return

Only use the public api, this allows you to change the internal state

### explain splitting of implementation, servers

https://pragdave.me/blog/2017/07/13/decoupling-interface-and-implementation-in-elixir.html

### steps:

1.  super simple pomodoro, and everything explictit
2.  change to impl and defdelegate
3.  implement server
4.  change the tests for impl and add tests for pomodoro
5.  add a registry timer
6.  create a real timer

### generating a nerves app:

The https://hexdocs.pm/nerves/getting-started.html#content is very valuable
(don't forget the ssh-askpass)

```
mix nerves.new fw
cd fw
export MIX_TARGET=rpi3
mix deps.get
mix firmware
mix firmware.burn
```

next we add [nerves_init_gadget](https://github.com/nerves-project/nerves_init_gadget)

### over wifi we need to add pomodoro.local to /etc/hosts

192.168.0.14 pomodoro.local

### create the upload.sh script

```
mix firmware.gen.script
```

add this code to the `upload.sh` script so it doesn't execute when the env variables for wifi are not set. (So you're not locked out)

```bash
if [ -z ${NERVES_NETWORK_PSK+x} ]; then echo
    echo "Network not set"
    exit -1;
fi
```

and changes `nerves.local` to `pomodor.local`

export variables for your network:

```
export NERVES_NETWORK_PSK=****
export NERVES_NETWORK_SSID=****
mix firmware
./upload.sh
```

after the reboot you should be able to ssh to the rpi3:

```
ssh pomodoro.local -p 8989
```

If you get `ssh: connect to host pomodoro.local port 8989: No route to host` something went wrong :-(

and then you can start a Elixir session with

`'Elixir.IEx':start().

To kill an ssh session you can do `press enter and then ~.`

### Add ringlogger

https://github.com/nerves-project/ring_logger

Then you can `RingLogger.attach` or `RingLogger.get` with `get` you can look at past logs.

### Adding pomodoro

By adding pomodoro as a dependency to `fw/mix.es` you can start Pomodor in fw. Test this with a remote ssh session

> A tip in between, want to see the documentation of a function? do `h Registry.lookup` for example

You can also see that the Pomodoro application is completely started by doing

```IEx
iex(7)> Registry.start_link(name: Pomodoro.Dispatcher, keys: :duplicate)
{:error, {:already_started, #PID<0.196.0>}}
```

### Lighting a led with the raspberry

#### writing a value:

- add elixirale
- simple scheme: GPIO pin 18, resistor, + pole led, - led, ground
- pinouts can be found here: https://pinout.xyz/#

install elixirale and connect via ssh:

```IEx
'Elixir.IEx':start().
alias ElixirALE.GPIO
{:ok, pid} = GPIO.start_link(18, :output)
GPIO.write(pid, 1)
GPIO.write(pid, 0)
```

#### Reading a value:

- scheme: GPIO pin 27 (input), button (2 sides of the pins are connected (http://www.arduinoclassroom.com/images/arduino101chapter4/Figure%203%20-%20Pushbutton%20Breadboard%20and%20Schematic%20Symbols.jpg)), other pin, V (5v -> elixirAle has the pull upresistors on high)

```IEx
'Elixir.IEx':start().
alias ElixirALE.GPIO
{:ok, pid27} = GPIO.start_link(27, :input)
GPIO.read(pid27)
0
# keep the button pressed
GPIO.read(pid27)
1
```

Keep reading like this is ofcourse not a very good way, but this is elixir, so we can receive a message

```IEx
'Elixir.IEx':start().
alias ElixirALE.GPIO
{:ok, pid27} = GPIO.start_link(27, :input)
GPIO.set_int(pid27, :both)
# press the button a few times
flush
```

When you press multiple times and then flush, you'll notice that you might get something like this:

```
# I pressed 3 times
flush
{:gpio_interrupt, 27, :rising}
{:gpio_interrupt, 27, :rising}
{:gpio_interrupt, 27, :falling}
{:gpio_interrupt, 27, :rising}
{:gpio_interrupt, 27, :falling}
{:gpio_interrupt, 27, :rising}
{:gpio_interrupt, 27, :falling}
:ok
```

So you get more signals than clicks (double risings etc), I'm not completely sure, but I guess that the measuring is more sensitive than the button, so you'll have to implement a debouncer if you want to make it a bit more sturdy.
A button ignoring time can be sufficient: https://github.com/tcoopman/rocket_man/blob/master/fw/lib/fw/button.ex

So next up is lighting a led when you push a button

### light a led on push a button

see test_led_push.ex
push the button and the led will burn.
This should give you everything to code pomodoro.

Small tip: you can read from a output pin as well to see if a led is on or off.

### implement the pomodoro timer

You have everything you need at this point (make sure to check the commits in between)
Add it to the application to have it started automatically. (some things can be in config)

### Adding a ui/api

`mix phx.new --no-brunch --no-ecto ui`

### blinking leds

- via led interface
- how to sync them at the same time? (one possible implementation, but maybe/probably not the best)
- how to test (behaviours)

### remove the logic in fw

At the current point in time, we still have some logic in `fw` that (1) doesn't really belong there, and (2) that makes it harder to test things. `Fw.Pomodoro` listens to input - button presses and output - state changes that are translated to turning leds on and off

_Assignment_:

Remove the logic from `fw` and create a central place where the pomodoro timer can be managed.

These are the things that should be managed at least:

- start and stop from different signals (via the api and via buttons - make sure that via buttons works first)

**EXTRA**:

- configure the time of the pomodoro timer via the api
- implement an extent function that works like this: press extend to extent the current time period by 20%, make sure that an acknowledgement signal is given to the user (via the api and leds)

## some general tips

- use the logger (RingLogger.attach)
- don't forget to close the ssh connection before upgrading (enter ~.)
- You cannot test everything local (ElixirALE for example) so some testing is necessary on the raspberry. If we have time we can look at some tested boundaries (contracts) that we can then mock locally. This will make it easier to test locally and be more sure that we don't break anything. Nerves makes it possible to switch between partions when you broke something (we don't have time to look at this, but for more info look at this post: http://embedded-elixir.com/post/2018-01-08-firmware-revert/)
- for testing, try to put as little logic in `fw` as possible. Things that are not coupled (like `pomodoro` itself) to fw are easier to test. The `fw` can be tested with mocks as well, something we will take a look at if the time allows for it. Also see: [mox](https://github.com/plataformatec/mox).
- if you pattern match on keyword list, make sure you keep the same order. For keywords lists you can better use the keyword module: https://hexdocs.pm/elixir/Keyword.html
- read the warnings

## TODO

- test on a windows vm: http://embedded-elixir.com/post/2018-01-08-windows-support/
- write assignments
- write down the rules
- create a cheat sheet
- have some writing materials
- have some post-its

## Cool things to notice

- during an upgrade, the program keeps working till you get to the reboot.

# Rules

- Do, don't ask!
- Changing direction is possible, if the whole group finds it valuable
- Schedule is there to be changed

# Cheat sheet

- use https://elixir-lang.org/getting-started/
- don't forget with
- optionally (= ask more about it) = debugging
- optionally (erlang)
- later (processes, file io)

# Searching for the devices

Should not be necessary if mDns is configured correctly

```
sudo nmap -PN 192.168.0.1-100 -p 8989 | grep -i "open" -B4
```

# Gists

https://gist.github.com/tcoopman/e9a3e6d960128c5bbf6cf544ffde1e24
