defmodule Pomodoro.ListenerTest do
  use ExUnit.Case

  setup do
    {:ok, _pid} = start_supervised({Registry, [keys: :duplicate, name: Pomodoro.Dispatcher]})
    Registry.register(Pomodoro.Dispatcher, :listener, [])

    {:ok, _pomodoro} =
      start_supervised(%{
        id: Pomodoro,
        start: {Pomodoro, :start_link, []}
      })

    :ok
  end

  test "a state change is reported" do
    advance_time(5)
    advance_time(5)
    advance_time(5)
    advance_time(5)
    advance_time(5)

    refute_receive({:state_changed, :working})
    assert_receive({:state_changed, :pauzing})
  end

  test "multiple state changes reported" do
    advance_time(25)
    advance_time(5)

    assert_receive({:state_changed, :pauzing})
    assert_receive({:state_changed, :working})
  end

  defp advance_time(time) do
    Registry.dispatch(Pomodoro.Dispatcher, :timer, fn listeners ->
      for {pid, _} <- listeners do
        send(pid, {:advance_time, time})
      end
    end)
  end
end
