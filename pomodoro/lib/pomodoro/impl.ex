defmodule Pomodoro.Impl do
  @moduledoc """
  Documentation for Pomodoro.
  """

  @doc """
  Start a new pomodoro timer
  """
  def start do
    {:working, 1}
  end

  @doc """
  Check if the state is in working
  """
  def state({state, _}), do: state

  def same_state({state1, _}, {state2, _}) when state1 == state2, do: true
  def same_state(_, _), do: false

  def advance_time({:working, minutes}, time) do
    cond do
      minutes + time > 25 ->
        advance_time({:pauzing, 1}, minutes + time - 25)

      true ->
        {:working, minutes + time}
    end
  end

  def advance_time({:pauzing, minutes}, time) do
    cond do
      minutes + time > 5 ->
        advance_time({:working, 1}, minutes + time - 5)

      true ->
        {:pauzing, minutes + time}
    end
  end
end
