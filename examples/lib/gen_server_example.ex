defmodule GenServerExample do
  use GenServer

  def start_link(_) do
    GenServer.start_link(__MODULE__, nil)
  end

  def init(nil) do
    {:ok, 0}
  end

  def plus_one(pid) do
    GenServer.call(pid, :plus_one)
  end

  # def plus_two(pid) do
  #   GenServer.cast(pid, :plus_two)
  # end

  # def start_counter(pid) do
  #   GenServer.cast(pid, :start)
  # end

  # def state(pid) do
  #   GenServer.call(pid, :state)
  # end

  def handle_call(:plus_one, _from, state) do
    {:reply, :ok, state + 1}
  end

  # def handle_call(:state, _from, state) do
  #   {:reply, state, state}
  # end

  # def handle_cast(:plus_two, state) do
  #   {:noreply, state + 2}
  # end

  # def handle_cast(:start, state) do
  #   schedule_work()
  #   {:noreply, state}
  # end

  # def handle_info(:work, state) do
  #   schedule_work()
  #   {:noreply, state + 1}
  # end

  # defp schedule_work() do
  #   Process.send_after(self(), :work, 2 * 1_000)
  # end
end
