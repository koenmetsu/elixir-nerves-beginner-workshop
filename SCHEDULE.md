# Schedule

- 09:00 - 09:10: installing
- 09:10 - 09:20: short introduction
- 09:20 - 09:30: goals and applications
- 09:30 - 09:40: starting decisions
- 09:40 - 09:50: talk about elixir and nerves
- 09:50 - 10:10: syntax
- 10:10 - 10:50: assignment 1, pomodoro logic
- 10:50 - 11:10: break
- 11:10 - 11:25: introduction to mix
- 11:25 - 12:30: assignment 2, pomodoro logic test driven
- 12:30 - 13:30: lunch
- 13:30 - 13:50: genServers
- 13:50 - 15:00: assignment 3, genservers
- 15:00 - 16:00: assignment 4, Nerves
- 16:00 - 17:00: assignment 5, Pomodoro on Nerves
- 17:00 - 18:30: free choice

## 09:00 - 09:10 Installing of participants

- Laptop, power, writing materials, schedule

## 09:10 - 09:20 Explain

The goal of the workshop for me is:

- You'll have a good grasp of Elixir and Nerves
- Your goals have been met
- We have fun
- We learned things

This workshop will be hands-on. I'll try do as little as possible and give you as much work as possible.

Very short rules

- Changing direction is possible, if the whole group finds it valuable
- Schedule is there to be changed

## 09:20 - 09:30 goals and applications

- Write down 2 personal goals for the workshop (should be met at the end of the workshop)
- In group: 2 group goals
- Write down at least 5 possible applications with the RPI, leds,...
- In group: refine your applications

## 09:30 - 09:40 Starting decisions

We're a very small group, so we'll make some decisions:

1. 2 possibilities, everyone at their own laptop
2. Mob

This is your choice, but try to choose based on your personal goals first, second the group goals.
Whatever we do, helping each other out will be very important.

2 possible ways: more in depth at the beginning and less done at the end, or less in depth in the beginning and further. We can change this during the workshop

## 09:40 - 09:50 Talk about Elixir and Nerves

### Elixir

- Functional language
- Dynamic and compiled
- Pattern matching, immutability
- The Erlang VM (BEAM) - highly distributed fault-tolerant systems

### Nerves

- Bulletproof embedded software in Elixir
- Minimal firmware for devices
- With the full power of Elixir

### Why learn it (Short)

- Best (arguably) implementation of _Agents_
- Because learning something new is great
- Really easy to have good concurrency

## 09:50 - 10:10 Syntax

- Use the cheat sheet
- Show a simple life example with `exs`

## 10:10 - 10:50: First assignment, pomodoro Logic

### Assignment

**If we want to go faster, we would be better of combining the next 2 assignments**

- Tips:
  - read all the instructions up front
  - make sure you always have `iex` open to test things.
  - If you struggle, don't waste time, ask
- Goal:
  - play and familiarize with the syntax. Any issue: ask
  - We will throw away this code at the end of the session
  - At any time if you feel relatively comfortable, raise your hand and we will continue to the next part
- Task:
  - Write a module `PomodoroLogic` in the file `pomodoro.exs`
  - Run with `elixir pomodoro.exs`
  - Test with `IO.puts` and `IO.inspect` (try `IO.puts(%{})`) at the end of the file
  - With a function start, that should return a `tupple` {:ok, state} (the state can probably be a `struct`)
  - With a function `is_working?` that returns `true` when working, `false` otherwise
  - With a function `is_pausing?` that returns `true` when pauzing, `false` otherwise
  - With constant time intervals, 25 minutes of working, 5 minutes of pauze
  - With a function `advance_time` that given a number of minutes will advance the time and return the new state. (This is a good candidate for a recursive function)
- Extra tasks:
  - Make it possible for the constant intervals to be overwritten
  - Count how many intervals have been passed and be able to query that information
  - Add extra functions that return the time before the next break or the next start of a working session
- You should have used at least these things:
  - constant
  - recursion
  - pattern matching with guards
  - struct
  - `case` or `cond`
  - `|>` pipeline

## 11:10 - 11:25: Introduction to mix

```bash
mix new pomodoro
cd pomodoro
mix test
mix help
mix help test
```

Explore the project:

- show what has been created
- mix.exs
- tests
- lib
- config

## 11:25 - 12:30: Second assignment pomodoro logic test driven

- Goal:
  - Getting more fluent with Elixir
  - Do it again, but better
  - You're allowed to change the API
- Task: redo the logic, but do it test driven
- GWT (as guidelines):

```cucumber
WHEN I start a pomodoro timer
THEN the pomodoro timer is in working

GIVEN I started a pomodoro timer
WHEN 24 minutes have passed
THEN the pomodoro timer is in working

GIVEN I started a pomodoro timer
WHEN 25 minutes have passed
THEN the pomodoro timer is in pauzing

GIVEN I have a pomodoro timer in pauzing
WHEN 5 minutes have passed
THEN the pomodoro timer is in working

GIVEN I start a pomodoro timer
WHEN 31 minutes have passed
THEN the pomodoro timer is in working
```

*optional scenario (stopping):*

You can't do any actions on a stopped state, only query/inspect it.

```cucumber
GIVEN I have a pomodoro timer in working
WHEN I stop the pomodoro timer
THEN the pomodoro timer is stopped

GIVEN I have a pomodoro timer in pauzing
WHEN I stop the pomodoro timer
THEN the pomodoro timer is stopped
```

*optional scenario (postpone):*

Postponing will extend the current session with the given time.

```cucumber
GIVEN I have a pomodoro timer in working for 24 minutes
WHEN I postpone the timer for 5 minutes
AND 1 minute is passed (25 minutes in total)
THEN the pomodoro timer is in working

GIVEN I have a pomodoro timer in working for 24 minutes
WHEN I postpone the timer for 5 minutes
AND 6 minutes are passed (30 minutes in total)
THEN the pomodoro timer is in pauzing

GIVEN I have a pomodoro timer in pauzing for 3 minutes
WHEN I postpone the timer for 2 minutes
AND 3 minutes are passed (6 minutes in total)
THEN the pomodoro timer is in pauzing

GIVEN I have a pomodoro timer in pauzing for 3 minutes
WHEN I postpone the timer for 2 minutes
AND 4 minutes are passed (7 minutes in total)
THEN the pomodoro timer is in working
```

## 12:30 - 13:30 Lunch

During lunch, evaluation, do we have to change things

## 13:30 - 13:50: GenServers

So far: only single process = no concurrency at all.

### Lightweight processes

In an `iex` session:

```elixir
spawn fn -> 1 + 1 end
pid = spawn fn -> 1 + 1 end
Process.alive?(pid)
pid = spawn fn -> Process.sleep(1000 * 5) end
Process.alive?(pid)
self()
send(self(), :hello)
flush()
receive do
    :hello -> IO.puts "YES"
end
spawn_link fn -> raise "oops" end
```

### GenServers

https://hexdocs.pm/elixir/GenServer.html

- use GenServer
- @impl true

```elixir
use GenServer

GenServer.start_link(__MODULE__, state, opts)

cast
call
info
```

- See `examples/lib/gen_server_example.ex`

```elixir
iex(1)> {:ok, pid} = GenServerExample.start_link([])
{:ok, #PID<0.144.0>}
iex(2)> :sys.statistics(pid, true)
:ok
iex(3)> :sys.trace(pid, true)
:ok
iex(4)> GenServerExample.plus_one(pid)
*DBG* <0.144.0> got call plus_one from <0.142.0>
*DBG* <0.144.0> sent ok to <0.142.0>, new state 1
:ok
iex(8)> recompile() # after adding state function
Compiling 1 file (.ex)
:ok
iex(9)> GenServerExample.state(pid)
*DBG* <0.144.0> got call state from <0.142.0>
*DBG* <0.144.0> sent 1 to <0.142.0>, new state 1
1
iex(10)> recompile() # add plus 2 cast
Compiling 1 file (.ex)
:ok
iex(11)> GenServerExample.plus_two(pid)
*DBG* <0.144.0> got cast plus_two
:ok
*DBG* <0.144.0> new state 3
iex(12)> :sys.statistics(pid, :get)
{:ok,
 [
   start_time: {{2018, 6, 17}, {11, 49, 3}},
   current_time: {{2018, 6, 17}, {11, 53, 27}},
   reductions: 376,
   messages_in: 3,
   messages_out: 0
 ]}
iex(13)> :sys.get_status(pid)
{:status, #PID<0.144.0>, {:module, :gen_server},
 [
   [
     "$initial_call": {GenServerExample, :init, 1},
     "$ancestors": [#PID<0.142.0>, #PID<0.58.0>]
   ],
   :running,
   #PID<0.142.0>,
   [
     trace: true,
     statistics: {{{2018, 6, 17}, {11, 49, 3}}, {:reductions, 36}, 3, 0}
   ],
   [
     header: 'Status for generic server <0.144.0>',
     data: [
       {'Status', :running},
       {'Parent', #PID<0.142.0>},
       {'Logged events', []}
     ],
     data: [{'State', 3}]
   ]
 ]}
iex(14)> recompile()
Compiling 1 file (.ex)
:ok
iex(15)> GenServerExample.start_counter(pid)
*DBG* <0.144.0> got cast start
:ok
*DBG* <0.144.0> new state 3
*DBG* <0.144.0> got work
*DBG* <0.144.0> new state 4
iex(16)> recompile() # add start_counter
*DBG* <0.144.0> got work
*DBG* <0.144.0> new state 5
Compiling 1 file (.ex)
:ok
*DBG* <0.144.0> got work
*DBG* <0.144.0> new state 6
*DBG* <0.144.0> got work
*DBG* <0.144.0> new state 7
*DBG* <0.144.0> got work
*DBG* <0.144.0> new state 8
*DBG* <0.144.0> got work
*DBG* <0.144.0> new state 9
*DBG* <0.144.0> got work
*DBG* <0.144.0> new state 10
```

## 13:50 - 15:00: assignment 3, genservers

- Goal: understand and use GenServers
- Task:
  - wrap your pomodoro state in a GenServer
  - a minute can be one second for now
  - test your GenServer (don't forget `handle_xxx` are just methods as well)
- Extra:
  - Your GenServer should listen to a message to advance the time (so time is not part of your GenServer implementation)
  - Adding the Registry to dispatch messages (ask about this!)

## 15:00 - 16:00: assignment 4, nerves

- Goal:
  - have a working nerves
  - manually be able to light a led
  - have a working button
  - toggle a led when a button is pressed
- Task: follow the steps
- Tip: you can peek in `fw_example` if needed

- Step 1: new nerves project

```bash
mix nerves.new fw
cd fw
export MIX_TARGET=rpi3 # We're working with a rpi3
mix deps.get
mix firmware
```

next we add [nerves_init_gadget](https://github.com/nerves-project/nerves_init_gadget)

Add nerves_init_gadget to the deps in the `mix.exs`:

```elixir
  defp deps(target) do
    [
      {:nerves_runtime, "~> 0.4"},
      {:nerves_init_gadget, "~> 0.3"}
    ] ++ system(target)
  end
```

Now add nerves_init_gadget to the list of applications to always start. Change `config/config.exs` and edit the `:shoehorn` config to look something like this:

```elixir
config :shoehorn,
  init: [:nerves_runtime, :nerves_init_gadget],
  app: Mix.Project.config()[:app]
```

Next, set up the ssh authorized keys for pushing firmware updates to the device.

```elixir
config :nerves_firmware_ssh,
  authorized_keys: [
    File.read!(Path.join(System.user_home!, ".ssh/id_rsa.pub"))
  ]
```

And setup the wifi configuration

```elixir
key_mgmt = System.get_env("NERVES_NETWORK_KEY_MGMT") || "WPA-PSK"

config :nerves_network, :default,
  wlan0: [
    ssid: System.get_env("NERVES_NETWORK_SSID"),
    psk: System.get_env("NERVES_NETWORK_PSK"),
    key_mgmt: String.to_atom(key_mgmt)
  ]
```

(For here you could also set the SSID and PSK fixed)

And also add the configuration for mDNS (on **Archlinux**, be sure to follow these [Hostname resolution instructions](https://wiki.archlinux.org/index.php/avahi#Hostname_resolution) - on Mac this should just work)

```elixir
config :nerves_init_gadget,
  ifname: "wlan0",
  address_method: :dhcp,
  mdns_domain: "nerves-thomas.local", #change your name!!!
  node_name: nil,
  node_host: :mdns_domain
```

And at the end change the default logger backend:

```elixir
config :logger, backends: [RingLogger]
```

Build and burn

```elixir
mix deps.get
mix firmware
export NERVES_NETWORK_SSID=INSERTHERE # Very important!
export NERVES_NETWORK_PSK=INSERTHERE # Very important!
mix firmware.burn
```

Test with `ssh nerves-NAME.local -p 8989`
If that works, you can start a remote session with:
`'Elixir.IEx':start().`

And then you can start playing around! A good idea is to first attach the RingLogger: `RingLogger.attach`

To stop you can type `~.` on a new line (this terminates an ssh connection)

Now that you played a bit, we're going to add [elixir_ale](https://github.com/fhunleth/elixir_ale) to be able to set the GPIO pins. You will need this: [pinout](https://pinout.xyz/)

add to `mix.exs` dependencies

{:elixir_ale, "~> 1.0"}

then do

```bash
mix deps.get
mix firmware
./upload.sh # you will need to change nerves.local to your ip address
```

(If you don't want to wait for this, you can also run mix firmware.burn and restart your device with the updated sd card)

Wait for a bit and connect again via ssh

### Let's turn on a led

#### Build the board

1. Make sure you have a resistor (330ohm) between your led and the connection
2. Put the negative pole (shortest leg) to the ground (always connect the ground first)
3. Put the positive led (longest leg) to a green GPIO pin from pinout. (The BCM number has to be used)

#### Turn on the led

Connect with `elixir_ale` installed and start `IEx`

```elixir
alias ElixirALE.GPIO
{:ok, pid} = GPIO.start_link(18, :output) # make sure to pick your pin
GPIO.write(pid, 1)
GPIO.write(pid, 0)
```

### Pressing a button

#### Build the board

1. one pin to 5V
2. other pin to green GPIO

```elixir
{:ok, button_pid} = GPIO.start_link(17, :input)
GPIO.read(button_pid) # check what happens when you keep the button pressed
# We can also receive messages instead of waiting
GPIO.set_int(button_pid, :both)
flush # press the button see what happens - do you still remember how you can receive these messages?
```

### Toggling the led when you press the button

For this you should write some code, but you should have everything you need to get this done.

## 16:00 - 17:00: Assignment 5, Pomodoro on Nerves

You should have almost all information necessary, but here are still some important pointers.

- Add `pomodoro` as a dependency to `fw`:

In `mix.exs` of `fw` in the `deps`:

```elixir
{:pomodoro, path: "../pomodoro"},
```

* Using the Registry as a dispatcher would be a good way to communicate

In your `pomodoro` `application.ex` add to the children:

```elixir
{Registry, [keys: :duplicate, name: Pomodoro.Dispatcher]}
```

This can be tested with an `iex -S mix` session:

```elixir
iex(1)> Registry.register(Pomodoro.Dispatcher, :listener, [])
{:ok, #PID<0.161.0>}
iex(2)> Registry.dispatch(Pomodoro.Dispatcher, :listener, fn listeners -> for {pid, _} <- listeners, do: send(pid, :info) end)
:ok
iex(3)> flush()
:info
:ok
```

This should be enough information to continue, for inspiration, look at the code

## 17:00 - 18:00: free choice, continue