defmodule Fw.PushALed do
  use GenServer
  alias ElixirALE.GPIO

  def start_link([button_pin: _, led_pin: _] = opts) do
    GenServer.start_link(__MODULE__, opts)
  end

  @impl GenServer
  def init(button_pin: button_pin, led_pin: led_pin) do
    {:ok, button_pid} = GPIO.start_link(button_pin, :input)
    GPIO.set_int(button_pid, :rising)
    {:ok, led_pid} = GPIO.start_link(led_pin, :output)
    {:ok, %{button_pid: button_pid, led_pid: led_pid}}
  end

  @impl GenServer
  def handle_info({:gpio_interrupt, _, :rising}, %{led_pid: led_pid} = state) do
    GPIO.write(led_pid, 1)
    {:noreply, state}
  end

  # even with listening to rising we still seem to get a falling, so we ignore these inputs for now
  def handle_info({:gpio_interrupt, _, _}, state), do: {:noreply, state}
end
