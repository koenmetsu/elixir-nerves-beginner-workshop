defmodule Fw.Application do
  use Application
  require Logger

  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    # Define workers and child supervisors to be supervised
    children = [
      {Fw.Pomodoro, [button_on_pin: 23, button_off_pin: 22, working_pin: 18, pauzing_pin: 17]}
      # worker(Fw.Worker, [arg1, arg2, arg3]),
    ]

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Fw.Supervisor]

    Logger.info("starting the supervisor")

    Supervisor.start_link(children, opts)
  end
end
